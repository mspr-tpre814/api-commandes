package com.mspr.apiorders.entities;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {
    @Test
    public void testOrderEntity() {
        String id = "order123";
        Instant createdAt = Instant.now();
        String customerId = "customer123";
        List<String> products = Arrays.asList("product1", "product2");

        Order order = new Order();
        order.setId(id);
        order.setCreatedAt(createdAt);
        order.setCustomerId(customerId);
        order.setProducts(products);

        assertEquals(id, order.getId());
        assertEquals(createdAt, order.getCreatedAt());
        assertEquals(customerId, order.getCustomerId());
        assertEquals(products, order.getProducts());

        Order order2 = new Order(id, createdAt, customerId, products);
        assertEquals(id, order2.getId());
        assertEquals(createdAt, order2.getCreatedAt());
        assertEquals(customerId, order2.getCustomerId());
        assertEquals(products, order2.getProducts());
    }
}
