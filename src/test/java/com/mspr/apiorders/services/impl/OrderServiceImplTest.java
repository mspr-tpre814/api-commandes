package com.mspr.apiorders.services.impl;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.mspr.apiorders.entities.Order;
import com.mspr.apiorders.repositories.OrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderServiceImpl orderService;

    private Order order;

    @Before
    public void setUp() {
        order = new Order("1", Instant.now(),  "1", List.of("100"));
    }

    @Test
    public void getOrdersTest() {
        when(orderRepository.findAll()).thenReturn(Collections.singletonList(order));
        List<Order> orders = orderService.getOrders();
        assertFalse(orders.isEmpty());
        assertEquals(orders.get(0).getCustomerId(), "1");
    }

    @Test
    public void getOrderTest() {
        when(orderRepository.findById(anyString())).thenReturn(Optional.of(order));
        Order foundOrder = orderService.getOrder("1");
        assertNotNull(foundOrder);
        assertEquals(foundOrder.getCustomerId(), "1");
    }

    @Test
    public void updateOrderTest() {
        when(orderRepository.save(any(Order.class))).thenReturn(order);
        Order updatedOrder = orderService.updateOrder("1", order);
        assertNotNull(updatedOrder);
        assertEquals(updatedOrder.getCustomerId(), "1");
    }

    @Test
    public void updateOrderWithInvalidCustomerIdTest() {
        Order invalidOrder = new Order("1", Instant.now(),  "invalid", List.of("100"));
        when(orderRepository.save(any(Order.class))).thenThrow(DataIntegrityViolationException.class);
        try {
            orderService.updateOrder("1", invalidOrder);
            fail("Expected DataIntegrityViolationException");
        } catch (DataIntegrityViolationException e) {
            verify(orderRepository, times(1)).save(invalidOrder);
        }
    }

    @Test
    public void deleteOrderTest() {
        doNothing().when(orderRepository).deleteById(anyString());
        orderService.deleteOrder("1");
        verify(orderRepository, times(1)).deleteById("1");
    }

    @Test
    public void getOrderNotFoundTest() {
        when(orderRepository.findById(anyString())).thenReturn(Optional.empty());
        try {
            orderService.getOrder("1");
            fail("Expected NoSuchElementException");
        } catch (NoSuchElementException e) {
            verify(orderRepository, times(1)).findById("1");
        }
    }

    @Test
    public void updateOrderValidationErrorTest() {
        Order invalidOrder = new Order("", Instant.now(),  "", List.of("100"));

        when(orderRepository.save(any(Order.class))).thenThrow(DataIntegrityViolationException.class);

        try {
            orderService.updateOrder("1", invalidOrder);
            fail("Expected DataIntegrityViolationException");
        } catch (DataIntegrityViolationException e) {
            verify(orderRepository, times(1)).save(invalidOrder);
        }
    }
}
