package com.mspr.apiorders.api.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mspr.apiorders.api.controller.OrderController;
import com.mspr.apiorders.api.dto.OrderRequestDTO;
import com.mspr.apiorders.api.dto.OrderResponseDTO;
import com.mspr.apiorders.api.mapper.Mapper;
import com.mspr.apiorders.entities.Order;
import com.mspr.apiorders.services.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    @MockBean
    private Mapper mapper;

    private Order order;
    private OrderResponseDTO orderResponseDTO;
    private OrderRequestDTO orderRequestDTO;

    @Before
    public void setUp() {
        order = new Order("1", Instant.now(),  "1", List.of("100"));
        orderResponseDTO = new OrderResponseDTO("1", Instant.now(),  "1", List.of("100"));
        orderRequestDTO = new OrderRequestDTO("1", List.of("100"));
    }

    @Test
    public void getAllOrdersTest() throws Exception {
        List<Order> orderList = Collections.singletonList(order);
        List<OrderResponseDTO> orderResponseDTOList = Collections.singletonList(orderResponseDTO);

        when(orderService.getOrders()).thenReturn(orderList);
        when(mapper.toDTO(order)).thenReturn(orderResponseDTO);

        mockMvc.perform(get("/orders/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is("1")))
                .andExpect(jsonPath("$[0].customerId", is("1")));
    }

    @Test
    public void getOrderTest() throws Exception {
        when(orderService.getOrder(anyString())).thenReturn(order);
        when(mapper.toDTO(order)).thenReturn(orderResponseDTO);

        mockMvc.perform(get("/orders/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is("1")))
                .andExpect(jsonPath("$.customerId", is("1")));
    }

    @Test
    public void createOrderTest() throws Exception {
        when(orderService.createOrder(any(Order.class))).thenReturn(order);
        when(mapper.toDTO(any(Order.class))).thenReturn(orderResponseDTO);
        when(mapper.toEntity(any(OrderRequestDTO.class))).thenReturn(order);

        mockMvc.perform(post("/orders/")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(new ObjectMapper().registerModule(new JavaTimeModule()).writeValueAsString(orderResponseDTO)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is("1")))
                .andExpect(jsonPath("$.customerId", is("1")));
    }

    @Test
    public void updateOrderTest() throws Exception {
        when(orderService.updateOrder(anyString(), any(Order.class))).thenReturn(order);
        when(mapper.toDTO(any(Order.class))).thenReturn(orderResponseDTO);
        when(mapper.toEntity(any(OrderRequestDTO.class))).thenReturn(order);

        mockMvc.perform(put("/orders/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(new ObjectMapper().registerModule(new JavaTimeModule()).writeValueAsString(orderResponseDTO)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is("1")))
                .andExpect(jsonPath("$.customerId", is("1")));
    }

    @Test
    public void deleteOrderTest() throws Exception {
        mockMvc.perform(delete("/orders/1"))
                .andExpect(status().isOk());
    }
}
