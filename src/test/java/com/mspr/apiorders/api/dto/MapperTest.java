package com.mspr.apiorders.api.dto;

import com.mspr.apiorders.api.dto.OrderRequestDTO;
import com.mspr.apiorders.api.dto.OrderResponseDTO;
import com.mspr.apiorders.api.mapper.Mapper;
import com.mspr.apiorders.entities.Order;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class MapperTest {

    private final Mapper mapper = new Mapper();

    @Test
    public void testToEntity() {
        String customerId = "customer123";
        List<String> products = Arrays.asList("product1", "product2");

        OrderRequestDTO orderRequestDTO = new OrderRequestDTO(customerId, products);
        Order order = mapper.toEntity(orderRequestDTO);

        assertNotNull(order);
        assertEquals(customerId, order.getCustomerId());
        assertEquals(products, order.getProducts());
    }

    @Test
    public void testToDTO() {
        String id = "order123";
        Instant createdAt = Instant.now();
        String customerId = "customer123";
        List<String> products = Arrays.asList("product1", "product2");

        Order order = new Order(id, createdAt, customerId, products);
        OrderResponseDTO orderResponseDTO = mapper.toDTO(order);

        assertNotNull(orderResponseDTO);
        assertEquals(id, orderResponseDTO.getId());
        assertEquals(createdAt, orderResponseDTO.getCreatedAt());
        assertEquals(customerId, orderResponseDTO.getCustomerId());
        assertEquals(products, orderResponseDTO.getProducts());
    }
}
