package com.mspr.apiorders.api.dto;

import com.mspr.apiorders.api.dto.OrderRequestDTO;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class OrderRequestDTOTest {

    @Test
    public void testOrderRequestDTO() {
        String customerId = "customer123";
        List<String> products = Arrays.asList("product1", "product2");

        OrderRequestDTO dto = new OrderRequestDTO();
        dto.setCustomerId(customerId);
        dto.setProducts(products);

        assertEquals(customerId, dto.getCustomerId());
        assertEquals(products, dto.getProducts());

        OrderRequestDTO dto2 = new OrderRequestDTO(customerId, products);
        assertEquals(customerId, dto2.getCustomerId());
        assertEquals(products, dto2.getProducts());
    }
}
