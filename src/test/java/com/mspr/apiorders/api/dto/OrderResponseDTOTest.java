package com.mspr.apiorders.api.dto;

import com.mspr.apiorders.api.dto.OrderResponseDTO;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderResponseDTOTest {

    @Test
    public void testOrderResponseDTO() {
        String id = "order123";
        Instant createdAt = Instant.now();
        String customerId = "customer123";
        List<String> products = Arrays.asList("product1", "product2");

        OrderResponseDTO dto = new OrderResponseDTO();
        dto.setId(id);
        dto.setCreatedAt(createdAt);
        dto.setCustomerId(customerId);
        dto.setProducts(products);

        assertEquals(id, dto.getId());
        assertEquals(createdAt, dto.getCreatedAt());
        assertEquals(customerId, dto.getCustomerId());
        assertEquals(products, dto.getProducts());

        OrderResponseDTO dto2 = new OrderResponseDTO(id, createdAt, customerId, products);
        assertEquals(id, dto2.getId());
        assertEquals(createdAt, dto2.getCreatedAt());
        assertEquals(customerId, dto2.getCustomerId());
        assertEquals(products, dto2.getProducts());
    }
}
