package com.mspr.apiorders.repository;

import com.mspr.apiorders.entities.Order;
import com.mspr.apiorders.repositories.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class OrderRepositoryTest {
    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void testSaveAndFindById() {
        String customerId = "customer123";
        List<String> products = Arrays.asList("product1", "product2");
        Instant createdAt = Instant.now();

        Order order = new Order();
        order.setCustomerId(customerId);
        order.setProducts(products);
        order.setCreatedAt(createdAt);

        Order savedOrder = orderRepository.save(order);
        assertNotNull(savedOrder.getId());

        Order foundOrder = orderRepository.findById(savedOrder.getId()).orElse(null);
        assertNotNull(foundOrder);
        assertEquals(customerId, foundOrder.getCustomerId());
        assertEquals(products, foundOrder.getProducts());
        assertEquals(createdAt, foundOrder.getCreatedAt());
    }

    @Test
    public void testUpdateOrder() {
        String customerId = "customer123";
        List<String> products = Arrays.asList("product1", "product2");
        Instant createdAt = Instant.now();

        Order order = new Order();
        order.setCustomerId(customerId);
        order.setProducts(products);
        order.setCreatedAt(createdAt);

        Order savedOrder = orderRepository.save(order);
        String newCustomerId = "customer456";
        savedOrder.setCustomerId(newCustomerId);

        Order updatedOrder = orderRepository.save(savedOrder);
        assertEquals(newCustomerId, updatedOrder.getCustomerId());
    }

    @Test
    public void testDeleteOrder() {
        String customerId = "customer123";
        List<String> products = Arrays.asList("product1", "product2");
        Instant createdAt = Instant.now();

        Order order = new Order();
        order.setCustomerId(customerId);
        order.setProducts(products);
        order.setCreatedAt(createdAt);

        Order savedOrder = orderRepository.save(order);
        orderRepository.deleteById(savedOrder.getId());

        Order foundOrder = orderRepository.findById(savedOrder.getId()).orElse(null);
        assertNull(foundOrder);
    }
}
