package com.mspr.apiorders.entities;


import com.mspr.apiorders.utils.StringListConverter;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

/**
 * Order
 * Order is the entity used to represent an order
 */

@Entity
@Table(name = "orders")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "created_at")
    private Instant createdAt;
    @Column(name = "customer_id")
    private String customerId;
    @Convert(converter = StringListConverter.class)
    @Column(name = "products", columnDefinition = "TEXT")
    private List<String> products;
}
