package com.mspr.apiorders.configuration;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class for RabbitMQ
 * This class is used to create the queues and the message converter
 */
@Configuration
public class RabbitMQConfig {
    @Bean
    public Queue customerPresenceQueue() {
        return new Queue("customerPresenceQueue", false);
    }

    @Bean
    public Queue productAvailabilityQueue() {
        return new Queue("productAvailabilityQueue", false);
    }

    @Bean
    public Queue productUpdateStockQueue() {
        return new Queue("productUpdateStockQueue", false);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public MessageConverter messageConverter() {
        return jsonMessageConverter();
    }
}
