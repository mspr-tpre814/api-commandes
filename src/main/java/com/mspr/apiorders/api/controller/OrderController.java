package com.mspr.apiorders.api.controller;

import com.mspr.apiorders.api.dto.OrderRequestDTO;
import com.mspr.apiorders.api.dto.OrderResponseDTO;
import com.mspr.apiorders.api.mapper.Mapper;
import com.mspr.apiorders.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


/**
 *  OrderController
 *  This class is the controller for the Order entity.
 *  It handles the CRUD operations for the Order entity.
 *  It is accessible from the /orders path.
 *
 *  @RestController annotation is used to create RESTful web services using Spring MVC.
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/orders", produces = APPLICATION_JSON_VALUE)
public class OrderController {
    private final OrderService orderService;
    private final Mapper mapper;

    /**
     * This method returns all the orders.
     *
     * @return ResponseEntity<List<OrderResponseDTO>> This returns the list of orders.
     */
    @GetMapping("/")
    public ResponseEntity<List<OrderResponseDTO>> getAllOrders() {
        List<OrderResponseDTO> orders = orderService.getOrders()
                .stream()
                .map(mapper::toDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    /**
     * This method returns an order by its id.
     *
     * @param id The id of the order.
     * @return ResponseEntity<OrderResponseDTO> This returns the order.
     */
    @GetMapping("/{id}")
    public ResponseEntity<OrderResponseDTO> getOrder(@PathVariable String id) {
        return new ResponseEntity<>(mapper.toDTO(orderService.getOrder(id)), HttpStatus.OK);
    }

    /**
     * This method creates an order.
     *
     * @param orderRequestDTO The order to create.
     * @return ResponseEntity<OrderResponseDTO> This returns the created order.
     */
    @PostMapping("/")
    public ResponseEntity<OrderResponseDTO> createOrder(@RequestBody OrderRequestDTO orderRequestDTO) {
        return new ResponseEntity<>(mapper.toDTO(orderService.createOrder(mapper.toEntity(orderRequestDTO))), HttpStatus.CREATED);
    }

    /**
     * This method updates an order.
     *
     * @param id The id of the order.
     * @param orderRequestDTO The order to update.
     * @return ResponseEntity<OrderResponseDTO> This returns the updated order.
     */
    @PutMapping("/{id}")
    public ResponseEntity<OrderResponseDTO> updateOrder(@PathVariable String id, @RequestBody OrderRequestDTO orderRequestDTO) {
        return new ResponseEntity<>(mapper.toDTO(orderService.updateOrder(id, mapper.toEntity(orderRequestDTO))), HttpStatus.OK);
    }

    /**
     * This method deletes an order.
     *
     * @param id The id of the order.
     * @return ResponseEntity<?> This returns an empty response.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteOrder(@PathVariable String id) {
        orderService.deleteOrder(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
