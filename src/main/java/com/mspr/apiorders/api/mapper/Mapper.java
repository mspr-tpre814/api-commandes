package com.mspr.apiorders.api.mapper;

import com.mspr.apiorders.api.dto.OrderRequestDTO;
import com.mspr.apiorders.api.dto.OrderResponseDTO;
import com.mspr.apiorders.entities.Order;
import org.springframework.stereotype.Component;

/**
 * Mapper class to convert OrderRequestDTO to Order and Order to OrderResponseDTO
 */

@Component
public class Mapper {
    /**
     * Convert OrderRequestDTO to Order
     * @param orderRequestDTO OrderRequestDTO
     * @return Order
     */
    public Order toEntity(OrderRequestDTO orderRequestDTO) {
        Order order = new Order();
        order.setCustomerId(orderRequestDTO.getCustomerId());
        order.setProducts(orderRequestDTO.getProducts());
        return order;
    }

    /**
     * Convert Order to OrderResponseDTO
     * @param order Order
     * @return OrderResponseDTO
     */
    public OrderResponseDTO toDTO(Order order) {
        OrderResponseDTO orderResponseDTO = new OrderResponseDTO();
        orderResponseDTO.setId(order.getId());
        orderResponseDTO.setCreatedAt(order.getCreatedAt());
        orderResponseDTO.setCustomerId(order.getCustomerId());
        orderResponseDTO.setProducts(order.getProducts());
        return orderResponseDTO;
    }

}
