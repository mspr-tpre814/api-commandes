package com.mspr.apiorders.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * OrderRequestDTO
 * OrderRequestDTO is the DTO used to create an order
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequestDTO {
    private String customerId;
    private List<String> products;
}
