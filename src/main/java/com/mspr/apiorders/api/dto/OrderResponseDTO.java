package com.mspr.apiorders.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

/**
 * OrderResponseDTO
 * OrderResponseDTO is the DTO used to return an order
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponseDTO {
    private String id;
    private Instant createdAt;
    private String customerId;
    private List<String> products;
}
