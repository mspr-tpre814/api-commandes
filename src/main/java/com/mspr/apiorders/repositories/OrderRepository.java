package com.mspr.apiorders.repositories;

import com.mspr.apiorders.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * OrderRepository
 * Repository for Order entity
 */

@Repository
public interface OrderRepository extends JpaRepository<Order, String> {
}
