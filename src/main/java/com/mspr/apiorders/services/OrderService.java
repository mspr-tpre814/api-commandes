package com.mspr.apiorders.services;

import com.mspr.apiorders.entities.Order;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * OrderService
 * Service for Order entity
 */

@Service
public interface OrderService {
    List<Order> getOrders();
    Order getOrder(String id);
    Order createOrder(Order order);
    Order updateOrder(String uid, Order order);
    void deleteOrder(String id);
    boolean checkCustomerPresence(String id);
    boolean checkProductAvailability(String id);
}
