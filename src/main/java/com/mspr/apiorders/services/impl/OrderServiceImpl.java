package com.mspr.apiorders.services.impl;

import com.mspr.apiorders.entities.Order;
import com.mspr.apiorders.repositories.OrderRepository;
import com.mspr.apiorders.services.OrderService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

/**
 * OrderServiceImpl
 * OrderServiceImpl is the implementation of OrderService
 */

@Component
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final RabbitTemplate rabbitTemplate;

    /**
     * Check if a customer is present
     * @param id the customer id
     * @return true if the customer is present, false otherwise
     */
    @Override
    public boolean checkCustomerPresence(String id) {
        Boolean isCustomerPresent = (Boolean) rabbitTemplate.convertSendAndReceive("customerPresenceQueue", id);
        return isCustomerPresent != null && isCustomerPresent;
    }

    /**
     * Check if a product is available
     * @param id the product id
     * @return true if the product is available, false otherwise
     */
    @Override
    public boolean checkProductAvailability(String id) {
        Boolean isProductAvailable = (Boolean) rabbitTemplate.convertSendAndReceive("productAvailabilityQueue", id);
        return isProductAvailable != null && isProductAvailable;
    }

    /**
     * Get all orders
     * @return the list of orders
     */
    @Override
    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    /**
     * Get an order by its id
     * @param id the order id
     * @return the order
     */
    @Override
    public Order getOrder(String id) {
        return orderRepository.findById(id).orElseThrow();
    }

    /**
     * Create an order
     * @param order the order to create
     * @return the created order
     */
    @Override
    @Transactional
    public Order createOrder(Order order) {
        if(!checkCustomerPresence(order.getCustomerId())) {
            throw new RuntimeException("Customer not found");
        }
        for (String productId : order.getProducts()) {
            if(!checkProductAvailability(productId)) {
                throw new RuntimeException("Product not available");
            }
        }
        for (String productId : order.getProducts()) {
            rabbitTemplate.convertAndSend("productUpdateStockQueue", productId);
        }
        order.setCreatedAt(Instant.now());
        return orderRepository.save(order);
    }

    /**
     * Update an order
     * @param uid the order id
     * @param order the order to update
     * @return the updated order
     */
    @Override
    public Order updateOrder(String uid, Order order) {
        return orderRepository.save(order);
    }

    /**
     * Delete an order
     * @param id the order id
     */
    @Override
    public void deleteOrder(String id) {
        orderRepository.deleteById(id);
    }


}
